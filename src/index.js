import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

import { createStore } from "redux";
import { Provider } from "react-redux";

//initial state
const globalState = {
  favoritePokemon: [],
};

//reducer
const rootReducer = (state = globalState, action) => {
  switch (action.type) {
    case "ADD_FAVORITE":
      return {
        ...state,
        favoritePokemon: state.favoritePokemon.concat(action.newValue),
      };
    case "REMOVE_FAVORITE":
      return {
        ...state,
        favoritePokemon: state.favoritePokemon.filter(
          (item) => item.id !== action.remove
        ),
      };
    default:
      break;
  }
  return state;
};

//store
const storeRedux = createStore(rootReducer);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={storeRedux}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
