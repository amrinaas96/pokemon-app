import React, { Component } from "react";
import { connect } from "react-redux";
import Pokemon from "./Pokemon";

class Favorite extends Component {
  render() {
    console.log(this.props);
    return (
      <div>
        <h1>Favorite Pokemon</h1>
        <Pokemon />
        <div>{this.props.favorite}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    favorite: state.favoritePokemon,
  };
};

export default connect(mapStateToProps)(Favorite);
