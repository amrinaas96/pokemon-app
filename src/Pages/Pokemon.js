import React, { Component } from "react";
import { connect } from "react-redux";
import "./Pokemon.css";

class Pokemon extends Component {
  constructor() {
    super();
    this.state = {
      pokemons: [],
      limit: 20,
      offset: 0,
      favorites: [],
      isLoading: true,
    };
  }

  componentDidMount() {
    this.fetchPokemon();
  }

  fetchPokemon = () => {
    const { limit, offset } = this.state;
    this.setState({ isLoading: true });
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
      .then((response) => response.json())
      .then((data) => this.setState({ pokemons: data.results, isLoading: false }))
      .catch((error) => console.log(error));
  };

  render() {
    console.log(this.props);
    const { pokemons, isLoading } = this.state;

    return (
      <div>
        <div>
          <h1>Pokemon Favorite</h1>
          <div className="pokemon-container">
            {this.props.favorite.length === 0 || isLoading ? (
              <div>You haven't choose your favorite Pokemon!</div>
            ) : (
              this.props.favorite.map((pokemon, index) => {
                return (
                  <div className="card-wrapper">
                    <div>
                      <img
                        src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
                          index + 1
                        }.png`}
                        alt={pokemon.name}
                      />
                      <div>{pokemon.name}</div>
                      <button
                        className="fav-button"
                        onClick={
                          () => {
                            this.props.handleRemove(pokemon.id);
                          }
                        }
                      >
                        Remove
                      </button>
                    </div>
                  </div>
                );
              })
            )}
          </div>
        </div>

        <div>
          <h1>Pokemon List</h1>
          <div className="pokemon-container">
            {
              pokemons.length === 0 || isLoading ? (
                <div>Loading</div>
              ) : (
                pokemons.map((item, index) => (
                  <div key={index} className="card-wrapper">
                    <img
                      src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
                        index + 1
                      }.png`}
                      alt={item.name}
                    />
                    <h2>{item.name}</h2>
                    <button
                      className="fav-button"
                      onClick={
                        (pokemon) => {
                          this.props.handleFavorite({
                            newValue: {
                              id: index + 1,
                              name: pokemon.name,
                            },
                          });
                        }
                      }
                    >
                      Add to Favorite
                    </button>
                  </div>
                ))
              )
            }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateTopProps = (state) => {
  return {
    favorite: state.favoritePokemon,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleFavorite: (param) =>
      dispatch({ type: "ADD_FAVORITE", newValue: param.newValue }),
    handleRemove: (param) =>
      dispatch({ type: "REMOVE_FAVORITE", remove: param }),
  };
};

export default connect(mapStateTopProps, mapDispatchToProps)(Pokemon);
