import React, { useState, useEffect } from "react";

export default function ContactUs() {
  const [name, setName] = useState(" ");
  const [message, setMessage] = useState(" ");
  const [developers, setDevelopers] = useState([
    { id: 1, name: "Doraemon" },
    { id: 2, name: "Shinchan" },
  ]);
  const [newDev, setNewDev] = useState(" ");

  const handleSubmit = (event) => {
    event.preventDefault();
    setDevelopers([
      ...developers,
      {
        id: developers.length + 1,
        name: newDev,
      },
    ]);
    setNewDev("");
  };

  useEffect(() => {
    fetchPokemon();
  }, [name, message]);

  const fetchPokemon = () => {
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=${10}&offset=${0}`)
      .then((response) => response.json())
      .then((data) => console.log(data));
  };

  return (
    <div>
      <h1>Page Contact Us</h1>

      <div>Namanya adalah : {name}</div>
      <div>Pesannya adalah : {message}</div>
      <h3>Full Name</h3>
      <input
        type="text"
        value={name}
        onChange={({ target: { value } }) => setName(value)}
      />

      <h3>Your Message</h3>
      <input
        type="text"
        value={message}
        onChange={({ target: { value } }) => setMessage(value)}
      />

      <div>Developers</div>
      {developers.map((developer) => (
        <div>{developer.name}</div>
      ))}

      <div>Add Developer</div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={newDev}
          onChange={({ target: { value } }) => setNewDev(value)}
        />
        <button type="submit">Tambah</button>
      </form>
    </div>
  );
}
