import React from "react";
import "./PokeDeck.css";
import PokeCard from "../Components/PokeCard";
import pokeData from "../Pokemon/pokeData.json";

export default function PokeDeck() {
  return (
    <div>
      <p className="title">Poke Apps</p>
      <div className="card-wraper">
        {pokeData.map((item) => (
          <PokeCard name={item.name} pic={item.img} />
        ))}
      </div>
    </div>
  );
}
