import React from "react";
import logo from "../Assets/pokeball-1.png";
import "./Navbar.css";

export default function Navbar() {
  return (
    <div className="container">
      <div className="logo">
        <a href="/">
          <img className="home-logo" src={logo} alt="logo" />
        </a>
      </div>
      <div className="navigation">
        <ul>
          {/* <li>
            <a href="/pokedex">Pokedex</a>
          </li>
          <li>
            <a href="/about">About</a>
          </li>
          <li>
            <a href="/list-pokemon">Pokemon list</a>
          </li>
          <li>
            <a href="/pokemon">Pokemon</a>
          </li> */}
          <li>
            <a href="/favorite">Favorite</a>
          </li>
          <li>
            <a href="/contact-us">Contact Us</a>
          </li>
        </ul>
      </div>
    </div>
  );
}
