import React, { useState, useEffect } from "react";

export default function HooksUseEffect() {
  const [namaPokemon, setNamaPokemon] = useState([]);

  useEffect(() => {
    fetchPokemon();
    console.log("Memanggil Use Effect");
  }, []);

  const fetchPokemon = () => {
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=${10}&offset=${0}`)
      .then((response) => response.json())
      .then((namaPokemon) => setNamaPokemon([namaPokemon]));
  };

  return (
    <div>
      <h2>Data Pokemon</h2>
      {namaPokemon.length === 0
        ? null
        : namaPokemon.map(() => <h1> Namanya adalah : {namaPokemon.name}</h1>)}
    </div>
  );
}
