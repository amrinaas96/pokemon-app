import React from "react";
import "./PokeCard.css";
import { useHistory } from "react-router-dom";

export default function PokeCard({ name, pic }) {
  const history = useHistory();
  return (
    <div
      className="card"
      onClick={() => {
        history.push("/poke-detail");
      }}
    >
      <img src={pic} alt="pikachu" />
      <p> {name} </p>
    </div>
  );
}
