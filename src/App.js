import Navbar from "./Components/Navbar";
import PokeDetail from "./Pages/PokeDetail";
import Pokemon from "./Pages/Pokemon";
// import PokeDeck from "./Pages/PokeDeck";
// import Pokedex from "./Pages/Pokedex";
// import About from "./Pages/About";
import ContactUs from "./Pages/ContactUs";
import Favorite from "./Pages/Favorite";
// import HooksUseEffect from "./Components/HooksUseEffect";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <Router>
      <Navbar />
      <Switch>
        <Route path="/" component={Pokemon} exact={true} />
        {/* <Route path="/pokemon" component={Pokemon} /> */}
        <Route path="/poke-detail" component={PokeDetail} exact />
        {/* <Route path="/list-pokemon" component={HooksUseEffect} exact /> */}
        {/* <Route path="/pokedex" component={Pokedex} /> */}
        {/* <Route path="/about" component={About} /> */}
        <Route path="/Favorite" component={Favorite} exact />
        <Route path="/contact-us" component={ContactUs} exact />
      </Switch>
    </Router>
  );
}

export default App;
