const redux = require("redux");
const createStore = redux.createStore;

const initialState = {
  favorite: 0,
  bestPokemon: null,
};

//reducer
const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_FAVORITE":
      return {
        ...state,
        favorite: state.favorite + 1,
      };
    case "SET_BEST_POKEMON":
      return {
        ...state,
        bestPokemon: action.valueBestPokemon,
      };
    default:
      break;
  }
  return state;
};

//store hubungin store dengan reducer
const store = createStore(rootReducer);

//subscribe
store.subscribe(() => {
  console.log(store.getState());
});

//dispatch /action --> object yang memiliki property TYPE, yang akan dikirim ke store dengan cara :
store.dispatch({ type: "ADD_FAVORITE" });
store.dispatch({ type: "SET_BEST_POKEMON", valueBestPokemon: "Pikachu" });

// Action --> Store --> Reducer
